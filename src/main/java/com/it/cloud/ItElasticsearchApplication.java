package com.it.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItElasticsearchApplication {

    public static void main(String[] args) {
        SpringApplication.run(ItElasticsearchApplication.class, args);
    }

}
