package com.it.cloud.es;

import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;

import java.io.IOException;
import java.sql.Date;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

/**
 * @author yangyang7_kzx
 * @date 2019/11/26 18:04
 * @description
 */
public class IndexService {

    @Resource(name = "dataClient")
    private Client client;

    public void index() throws IOException {
        IndexResponse response = client.prepareIndex("twitter", "tweet", "1")
                .setSource(jsonBuilder()
                        .startObject()
                        .field("user", "kimchy")
//                        .field("postDate", new Date())
                        .field("message", "trying out Elasticsearch")
                        .endObject()
                )
                .execute()
                .actionGet();
    }

}
