package com.it.cloud.es;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.global.Global;
import org.elasticsearch.search.aggregations.bucket.missing.Missing;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.bucket.nested.NestedAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.range.Range;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.avg.Avg;
import org.elasticsearch.search.aggregations.metrics.max.Max;
import org.elasticsearch.search.aggregations.metrics.percentiles.Percentile;
import org.elasticsearch.search.aggregations.metrics.percentiles.Percentiles;
import org.elasticsearch.search.aggregations.metrics.stats.Stats;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import org.elasticsearch.search.aggregations.metrics.tophits.TopHits;
import org.elasticsearch.search.aggregations.metrics.valuecount.ValueCount;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import javax.annotation.Resource;


/**
 * 查询测试
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class AggregationTest {

    @Resource(name = "dataClient")
    private TransportClient client;

    /**
     * 聚合--term aggregation
     */
    @Test
    public void aggregation() {
        SearchResponse sr = client.prepareSearch()
                .setQuery(QueryBuilders.matchAllQuery())
                .addAggregation(
                        AggregationBuilders.terms("agg1").field("name")
                )
                .execute().actionGet();

        Terms agg1 = sr.getAggregations().get("agg1");
        for (Terms.Bucket bucket : agg1.getBuckets()) {
            System.out.println(bucket.getKeyAsString());
        }

    }

    /**
     * 一次计算出count max min avg  sum
     */
    @Test
    public void stats() {
        SearchResponse response = client.prepareSearch("schools").setTypes("classes")
                .addAggregation(AggregationBuilders.stats("statsAgg").field("price"))
                .get();
        Stats ageAgg = response.getAggregations().get("statsAgg");
        System.out.println("总数：" + ageAgg.getCount());
        System.out.println("最小值：" + ageAgg.getMin());
        System.out.println("最大值：" + ageAgg.getMax());
        System.out.println("平均值：" + ageAgg.getAvg());
        System.out.println("和：" + ageAgg.getSum());
    }

    /**
     * countAgg 文档总数
     */
    @Test
    public void countAgg() {
        AggregationBuilder agg = AggregationBuilders.count("countAgg").field("name");

        SearchResponse response = client.prepareSearch("twitter").setTypes("tweet")
                .addAggregation(agg)
                .get();
        ValueCount valueCount = response.getAggregations().get("countAgg");
        long value = valueCount.getValue();
        System.out.println(value);
    }

    /**
     * sumAgg 总和
     */
    @Test
    public void sumAgg() {
        AggregationBuilder agg = AggregationBuilders.sum("sumAgg").field("postDate");

        SearchResponse response = client.prepareSearch("twitter").setTypes("tweet")
                .addAggregation(agg)
                .get();
        Sum valueCount = response.getAggregations().get("sumAgg");
        long value = (long) valueCount.getValue();
        System.out.println(value);
    }

    /**
     * maxAgg 最大数 ， 最小同理min
     */
    @Test
    public void maxAgg() {
        AggregationBuilder agg = AggregationBuilders.max("maxAgg").field("postDate");

        SearchResponse response = client.prepareSearch("twitter").setTypes("tweet")
                .addAggregation(agg)
                .get();
        Max valueCount = response.getAggregations().get("maxAgg");
        long value = (long) valueCount.getValue();
        System.out.println(value);
    }

    /**
     * avgAgg 平均
     */
    @Test
    public void avgAgg() {
        AggregationBuilder agg = AggregationBuilders.avg("avgAgg").field("postDate");

        SearchResponse response = client.prepareSearch("twitter").setTypes("tweet")
                .addAggregation(agg)
                .get();
        Avg valueCount = response.getAggregations().get("avgAgg");
        double value = valueCount.getValue();
        System.out.println(value);
    }


    /**
     * term 平均
     */
    @Test
    public void termAgg() {
        AggregationBuilder agg = AggregationBuilders.terms("Agg").field("name");
        // 计算每组的平均postDate指标
        // 子聚合
        agg.subAggregation(AggregationBuilders.avg("avgAgg")
                .field("postDate"));

        SearchResponse response = client.prepareSearch("twitter").setTypes("tweet")
                .addAggregation(agg)
                .get();
        Terms nameAgg = response.getAggregations().get("Agg");
        for (Terms.Bucket bucket : nameAgg.getBuckets()) {
            System.out.println(bucket.getKeyAsString());

            //取子聚合
            Avg avg = bucket.getAggregations().get("avgAgg");
            double value = avg.getValue();
            System.out.println(value);
        }
    }

    /**
     * 聚合排序，子聚合
     */
    @Test
    public void termAgg1() {
        AggregationBuilder agg = AggregationBuilders.terms("Agg").field("name")
                // 根据avgAgg排序
                .order(Terms.Order.aggregation("avgAgg", true));
        // .order(Terms.Order.count(true)); // 文档数排序
        // 计算每组的平均postDate指标
        // 子聚合
        agg.subAggregation(AggregationBuilders.avg("avgAgg")
                .field("postDate"));

        SearchResponse response = client.prepareSearch("twitter").setTypes("tweet")
                .addAggregation(agg)
                .get();
        Terms nameAgg = response.getAggregations().get("Agg");
        for (Terms.Bucket bucket : nameAgg.getBuckets()) {
            System.out.println(bucket.getKeyAsString());

            //取子聚合
            Avg avg = bucket.getAggregations().get("avgAgg");
            double value = avg.getValue();
            System.out.println(value);
        }
    }

    /**
     * Percentile Aggregation
     */
    @Test
    public void percentileAggregation() {
        AggregationBuilder agg =
                AggregationBuilders
                        .percentiles("agg")
                        .field("postDate")
                        .percentiles(1.0, 5.0, 10.0, 20.0, 30.0, 75.0, 95.0, 99.0);

        SearchResponse response = client.prepareSearch("twitter").setTypes("tweet")
                .addAggregation(agg)
                .get();

        Percentiles aggs = response.getAggregations().get("agg");
        for (Percentile entry : aggs) {
            double percent = entry.getPercent();    // Percent
            double value = entry.getValue();        // Value
            System.out.println("percent [{" + percent + "}], value [{" + value + "}]");
        }
    }

    /**
     * topHits
     */
    @Test
    public void topHits() {
        AggregationBuilder agg =
                AggregationBuilders
                        .terms("agg").field("name")
                        .subAggregation(
                                AggregationBuilders.topHits("top")
                                        // 排序
                                        .sort("postDate", SortOrder.ASC)
                                        .size(1)
                                        .from(0)
                                        // 返回字段
                                        .fetchSource("name", null)
                        );

        SearchResponse response = client.prepareSearch("twitter").setTypes("tweet")
                .addAggregation(agg)
                .get();

        Terms agg1 = response.getAggregations().get("agg");
        for (Terms.Bucket entry : agg1.getBuckets()) {
            String key = entry.getKeyAsString();                    // bucket key
            long docCount = entry.getDocCount();            // Doc count
            System.out.println("key [{" + key + "}], doc_count [{" + docCount + "}]");

            TopHits topHits = entry.getAggregations().get("top");
            for (SearchHit hit : topHits.getHits().getHits()) {
                System.out.println(" -> id [{" + hit.getId() + "}], _source [{" + hit.getSourceAsString() + "}]");
            }
        }
    }

    /**
     * global Aggregation
     */
    @Test
    public void globalAggregation() {
        BoolQueryBuilder boolBuilder = new BoolQueryBuilder();
        boolBuilder.must(QueryBuilders.termQuery("name", "SIMA"));

        AggregationBuilder agg = AggregationBuilders
                .global("agg")
                .subAggregation(AggregationBuilders.avg("avgAgg").field("postDate"));

        SearchResponse response = client.prepareSearch("twitter").setTypes("tweet")
                .setQuery(boolBuilder)
                .addAggregation(agg)
                .get();

        Global termAgg = response.getAggregations().get("agg");
        Avg avg = termAgg.getAggregations().get("avgAgg");
        System.out.println(termAgg.getDocCount());
        System.out.println(avg.getValue());
    }

    /**
     * filter Aggregation
     */
    @Test
    public void filterAggregation() {
        BoolQueryBuilder boolBuilder = new BoolQueryBuilder();
        boolBuilder.must(QueryBuilders.termQuery("name", "SIMA"));

        AggregationBuilder agg = AggregationBuilders
                .filter("agg", boolBuilder)
                .subAggregation(AggregationBuilders.avg("avgAgg").field("postDate"));

        SearchResponse response = client.prepareSearch("twitter").setTypes("tweet")
                .setQuery(boolBuilder)
                .addAggregation(agg)
                .get();

        Filter termAgg = response.getAggregations().get("agg");
        Avg avg = termAgg.getAggregations().get("avgAgg");
        System.out.println(termAgg.getDocCount());
        System.out.println(avg.getValue());
    }

    /**
     * range Aggregation
     */
    @Test
    public void rangeAggregation() {
        AggregationBuilder aggregation =
                AggregationBuilders
                        .range("agg")
                        .field("postDate")
                        .addUnboundedTo(1231356L)               // from -infinity to 1231356L (不包含1231356L)
                        .addRange(1231356L, 12345123123L)       // from 1231356L to 12345123123L (不包含12345123123L)
                        .addUnboundedFrom(12345123123L);

        SearchResponse response = client.prepareSearch("twitter").setTypes("tweet")
                .addAggregation(aggregation)
                .get();

        Range agg = response.getAggregations().get("agg");
        for (Range.Bucket entry : agg.getBuckets()) {
            String key = entry.getKeyAsString();            // Range as key
            Number from = (Number) entry.getFrom();          // Bucket from
            Number to = (Number) entry.getTo();              // Bucket to
            long docCount = entry.getDocCount();    // Doc count

            System.out.println("key [{" + key + "}], from [{" + from + "}], to [{" + to + "}], doc_count [{" + docCount + "}]");
        }

    }


   /* *//**
     * range Aggregation
     *//*
    @Test
    public void rangeAggregation() {
        AggregationBuilder aggregation =
                AggregationBuilders
                        .dateRange("agg")
                        .field("postDate")
                        .format("yyyy")
                        .addUnboundedTo("1950")    // from -infinity to 1950 (excluded)
                        .addRange("1950", "1960")  // from 1950 to 1960 (excluded)
                        .addUnboundedFrom("1960"); // from 1960 to +infinity

        SearchResponse response = client.prepareSearch("twitter").setTypes("tweet")
                .addAggregation(aggregation)
                .get();

        DateRange agg = response.getAggregations().get("agg");
        for (DateRange.Bucket entry : agg.getBuckets()) {
            String key = entry.getKey();                    // Date range as key
            DocValueFormat.DateTime fromAsDate = entry.getFromAsDate();    // Date bucket from as a Date
            DocValueFormat.DateTime toAsDate = entry.getToAsDate();        // Date bucket to as a Date
            long docCount = entry.getDocCount();            // Doc count

            logger.info("key [{}], from [{}], to [{}], doc_count [{}]", key, fromAsDate, toAsDate, docCount);
        }
    }*/

    /**
     * missing Aggregation
     */
    @Test
    public void missingAggregation() {
        AggregationBuilder aggregation = AggregationBuilders.missing("agg").field("price");
        SearchResponse response = client.prepareSearch("twitter").setTypes("tweet")
                .addAggregation(aggregation)
                .get();

        Missing agg = response.getAggregations().get("agg");
        System.out.println(agg.getDocCount());
    }

    /**
     * nested Aggregation
     */
    @Test
    public void nestedAggregation() {
        NestedAggregationBuilder aggregation = AggregationBuilders.nested("agg","users");
        SearchResponse response = client.prepareSearch("schools").setTypes("classes")
                .addAggregation(aggregation)
                .get();

        Nested agg = response.getAggregations().get("agg");
        System.out.println(agg.getDocCount());
    }

}
