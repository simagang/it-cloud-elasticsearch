package com.it.cloud.es;

import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.script.Script;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

/**
 * 索引测试
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class IndexTest {

    //    @Resource(name = "dataClient")
    @Autowired
    private TransportClient client;

    @Test
    public void index() throws IOException {
        IndexResponse response = client.prepareIndex("twitter", "tweet", "1")
                .setSource(jsonBuilder()
                        .startObject()
                        .field("name", "simagang")
                        .field("postDate", System.currentTimeMillis())
                        .field("message", "trying out Elasticsearch")
                        .endObject()
                )
                .execute()
                .actionGet();
    }

    @Test
    public void indexJson() {
        String json = "{" +
                "\"name\":\"yy\"," +
                "\"postDate\": 123456," +
                "\"message\":\"trying out Elasticsearch\"" +
                "}";

        IndexResponse response = client.prepareIndex("twitter", "tweet")
                .setSource(json)
                .execute()
                .actionGet();
    }

    @Test
    public void delete() {
        DeleteResponse response = client.prepareDelete("twitter", "tweet", "AW6nWQXDCJTyfWC03Cpb")
                .execute()
                .actionGet();
    }

    /**
     * 通过脚本更新-script
     */
    @Test
    public void updateByScript() throws ExecutionException, InterruptedException {
        UpdateRequest updateRequest = new UpdateRequest("twitter", "tweet", "1");
        updateRequest.script(new Script("ctx._source.name = \"sima\""));
        client.update(updateRequest).get();
    }

    /**
     * 部分更新-doc
     */
    @Test
    public void update() throws ExecutionException, InterruptedException, IOException {
        UpdateRequest updateRequest = new UpdateRequest("twitter", "tweet", "1")
                .doc(jsonBuilder()
                        .startObject()
                        .field("name", "yy")
                        .endObject());
        client.update(updateRequest).get();
    }

    /**
     * 存在就更新，不存在就添加
     */
    @Test
    public void upSert() throws ExecutionException, InterruptedException, IOException {
        IndexRequest indexRequest = new IndexRequest("twitter", "tweet", "1")
                .source(jsonBuilder()
                        .startObject()
                        .field("name", "Joe Smith")
                        .field("gender", "male")
                        .endObject());
        UpdateRequest updateRequest = new UpdateRequest("twitter", "tweet", "1")
                .doc(jsonBuilder()
                        .startObject()
                        .field("name", "upyy")
                        .endObject())
                .upsert(indexRequest);
        client.update(updateRequest).get();
    }

    /**
     * 批量请求-bulkApi
     */
    @Test
    public void bulkApi() throws ExecutionException, InterruptedException, IOException {
        BulkRequestBuilder bulkRequest = client.prepareBulk();

        bulkRequest.add(client.prepareIndex("twitter", "tweet", "1")
                .setSource(jsonBuilder()
                        .startObject()
                        .field("name", "kimchy1")
                        .field("postDate", 12345123123L)
                        .field("message", "trying out Elasticsearch")
                        .endObject()
                )
        );

        bulkRequest.add(client.prepareIndex("twitter", "tweet", "3")
                .setSource(jsonBuilder()
                        .startObject()
                        .field("name", "kimchy3")
                        .field("postDate", 12345123123L)
                        .field("message", "another post")
                        .endObject()
                )
        );

        BulkResponse bulkResponse = bulkRequest.execute().actionGet();
        if (bulkResponse.hasFailures()) {
            // process failures by iterating through each bulk response item
        }
    }

    /**
     * 批量处理器-BulkProcessor
     */
    @Test
    public void BulkProcessor() throws InterruptedException {
        BulkProcessor bulkProcessor = BulkProcessor.builder(
                // 添加您的elasticsearch客户端
                client,
                new BulkProcessor.Listener() {
                    // 在批量执行之前调用此方法。例如，您可以看到numberOfActions与 request.numberOfActions()
                    @Override
                    public void beforeBulk(long executionId,
                                           BulkRequest request) {
                    }

                    // 批量执行后调用此方法。您可以例如检查是否有一些失败的请求response.hasFailures()
                    @Override
                    public void afterBulk(long executionId,
                                          BulkRequest request,
                                          BulkResponse response) {
                    }

                    // 当批量失败并引发一个 Throwable
                    @Override
                    public void afterBulk(long executionId,
                                          BulkRequest request,
                                          Throwable failure) {
                    }
                })
                // 我们想每1万个请求执行一次批量处理
                // .setBulkActions(10000)
                // 我们要每1GB执行一次
                // .setBulkSize(new ByteSizeValue(1, ByteSizeUnit.GB))
                // 无论请求多少，我们都希望每5秒刷新一次
                .setFlushInterval(TimeValue.timeValueSeconds(5))
                // 设置并发请求数。值为0表示仅允许执行一个请求。值为1表示允许在累积新的批量请求时执行1个并发请求。
                .setConcurrentRequests(1)
                .build();

        IndexRequest request = new IndexRequest("twitter", "tweet", "2");
        // request.id("2");
        String jsonString = "{" +
                "\"name\":\"kimchy1\"," +
                "\"postDate\":1231356," +
                "\"message\":\"trying out Elasticsearch\"" +
                "}";
        request.source(jsonString);

        bulkProcessor.add(request);
        bulkProcessor.add(new DeleteRequest("twitter", "tweet", "3"));
        // 睡一会
        Thread.sleep(10000);
    }

}
