package com.it.cloud.es;

import org.apache.lucene.search.join.ScoreMode;
import org.assertj.core.util.Lists;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.ScriptQueryBuilder;
import org.elasticsearch.script.Script;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


/**
 * 查询测试
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class QueryTest {

    @Resource(name = "dataClient")
    private TransportClient client;

    @Test
    public void get() {
        GetResponse response = client.prepareGet("twitter", "tweet", "1")
                .setOperationThreaded(false)
                .execute()
                .actionGet();
        System.out.println(response.getSourceAsString());
    }

    /**
     * 滚动查询--scroll
     */
    @Test
    public void scroll() {
        // QueryBuilder qb = termQuery("name", "sima");

        SearchResponse scrollResp = client.prepareSearch("twitter")
                .setTypes("tweet")
                .setSearchType(SearchType.QUERY_THEN_FETCH)
                .setScroll(new TimeValue(60000))
                // .setQuery(qb)
                //100 hits per shard will be returned for each scroll
                .setSize(100).execute().actionGet();
        // 每次返回100条
        while (true) {
            for (SearchHit hit : scrollResp.getHits().getHits()) {
                System.out.println(hit.sourceAsString());
            }
            scrollResp = client.prepareSearchScroll(scrollResp.getScrollId()).setScroll(new TimeValue(600000)).execute().actionGet();
            //Break condition: No hits are returned
            if (scrollResp.getHits().getHits().length == 0) {
                break;
            }
        }
    }

    /**
     * 批量查询--MultiSearch
     */
    @Test
    public void MultiSearch() {
        SearchRequestBuilder srb1 = client.prepareSearch("twitter").setTypes("tweet")
                .setQuery(QueryBuilders.termQuery("name", "sima")).setSize(1);
        SearchRequestBuilder srb2 = client.prepareSearch("twitter").setTypes("tweet")
                .setQuery(QueryBuilders.matchQuery("name", "kimchy")).setSize(1);

        MultiSearchResponse sr = client.prepareMultiSearch()
                .add(srb1)
                .add(srb2)
                .execute().actionGet();

        // You will get all individual responses from MultiSearchResponse#getResponses()
        long nbHits = 0;
        for (MultiSearchResponse.Item item : sr.getResponses()) {
            SearchResponse response = item.getResponse();
            nbHits += response.getHits().getTotalHits();

            System.out.println(nbHits);
        }
    }

    /**
     * prefix前缀查询
     */
    @Test
    public void prefix() {
        //prefixQuery 第一个参数为字段名，后面是以kimchy开头的条件进行查询
        QueryBuilder builder = QueryBuilders.prefixQuery("name", "kimchy");
        SearchResponse response = client.prepareSearch("twitter")
                .setTypes("tweet")
                .setQuery(builder)
                .get();

        SearchHits hits = response.getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
            //将获取的值转换成map的形式
            Map<String, Object> map = hit.getSource();
            for (String key : map.keySet()) {
                System.out.println(key + " key对应的值为：" + map.get(key));
            }
        }
    }

    /**
     * wildcard查询
     */
    @Test
    public void wildcard() {
        QueryBuilder builder = QueryBuilders.wildcardQuery("name", "kimchy*");
        SearchResponse response = client.prepareSearch("twitter")
                .setTypes("tweet")
                .setQuery(builder)
                .get();

        SearchHits hits = response.getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
            //将获取的值转换成map的形式
            Map<String, Object> map = hit.getSource();
            for (String key : map.keySet()) {
                System.out.println(key + " key对应的值为：" + map.get(key));
            }
        }
    }

    /**
     * fuzzy模糊查询
     */
    @Test
    public void fuzzy() {
        QueryBuilder builder = QueryBuilders.fuzzyQuery("name", "kimcyh");
        SearchResponse response = client.prepareSearch("twitter")
                .setTypes("tweet")
                .setQuery(builder)
                .get();

        SearchHits hits = response.getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
            //将获取的值转换成map的形式
            Map<String, Object> map = hit.getSource();
            for (String key : map.keySet()) {
                System.out.println(key + " key对应的值为：" + map.get(key));
            }
        }
    }

    /**
     * term查询
     */
    @Test
    public void term() {
        QueryBuilder builder = QueryBuilders.termQuery("name", "kimchy1");
        SearchResponse response = client.prepareSearch("twitter")
                .setTypes("tweet")
                .setQuery(builder)
                .get();

        SearchHits hits = response.getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
            //将获取的值转换成map的形式
            Map<String, Object> map = hit.getSource();
            for (String key : map.keySet()) {
                System.out.println(key + " key对应的值为：" + map.get(key));
            }
        }
    }

    /**
     * terms查询
     */
    @Test
    public void terms() {
        List<Long> list = Lists.newArrayList();
        list.add(12345123123L);
        list.add(1231356L);

        QueryBuilder builder = QueryBuilders.termsQuery("postDate", list);
        SearchResponse response = client.prepareSearch("twitter")
                .setTypes("tweet")
                .setQuery(builder)
                .get();

        SearchHits hits = response.getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
            //将获取的值转换成map的形式
            Map<String, Object> map = hit.getSource();
            for (String key : map.keySet()) {
                System.out.println(key + " key对应的值为：" + map.get(key));
            }
        }
    }

    /**
     * idsQuery
     */
    @Test
    public void idsQuery() {
        QueryBuilder builder = QueryBuilders.idsQuery().addIds("1", "2");
        SearchResponse response = client.prepareSearch("twitter")
                .setTypes("tweet")
                .setQuery(builder)
                .get();

        SearchHits hits = response.getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
            //将获取的值转换成map的形式
            Map<String, Object> map = hit.getSource();
            for (String key : map.keySet()) {
                System.out.println(key + " key对应的值为：" + map.get(key));
            }
        }
    }

    /**
     * 限制返回字段和分页
     */
    @Test
    public void idsQuery1() {
        QueryBuilder builder = QueryBuilders.idsQuery().addIds("1", "2");
        SearchResponse response = client.prepareSearch("twitter")
                .setTypes("tweet")
                .setQuery(builder)
                // 分页
                .setFrom(0)
                .setSize(1)
                // 限制返回字段
                .setFetchSource("name", null)
                .get();

        SearchHits hits = response.getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
            //将获取的值转换成map的形式
            Map<String, Object> map = hit.getSource();
            for (String key : map.keySet()) {
                System.out.println(key + " key对应的值为：" + map.get(key));
            }
        }
    }

    /**
     * match查询，分词，适用于text类型的字段
     */
    @Test
    public void match() {
        QueryBuilder builder = QueryBuilders.matchQuery("title", "黑龙江");
        SearchResponse response = client.prepareSearch("twitter")
                .setTypes("tweet")
                .setQuery(builder)
                .get();

        SearchHits hits = response.getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
    }

    /**
     * multiMatch查询
     */
    @Test
    public void multiMatch() {
        // 搜索name或者title中含有“黑龙江”的文档
        QueryBuilder builder = QueryBuilders.multiMatchQuery("黑龙江", "name", "title");
        SearchResponse response = client.prepareSearch("twitter")
                .setTypes("tweet")
                .setQuery(builder)
                .get();

        SearchHits hits = response.getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
    }

    /**
     * range查询
     */
    @Test
    public void range() {
        QueryBuilder builder = QueryBuilders.rangeQuery("postDate")
                .from(1574874999000L)
                .to(15748749990001L)
                .includeLower(true) // 包含上届
                .includeUpper(false);// 包含下届

        SearchResponse response = client.prepareSearch("twitter")
                .setTypes("tweet")
                .setQuery(builder)
                .get();

        SearchHits hits = response.getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
    }

    /**
     * nested查询
     */
    @Test
    public void nestedMatch() {
        BoolQueryBuilder boolBuilder = new BoolQueryBuilder();
        boolBuilder.must(QueryBuilders.termQuery("users.name", "SIMA"));

        QueryBuilder builder = QueryBuilders.nestedQuery("users", boolBuilder, ScoreMode.None);
        SearchResponse response = client.prepareSearch("schools")
                .setTypes("classes")
                .setQuery(builder)
                .get();

        SearchHits hits = response.getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
    }

    /**
     * exists查询
     */
    @Test
    public void exists() {
        QueryBuilder builder =QueryBuilders.existsQuery("title");
        SearchResponse response = client.prepareSearch("twitter")
                .setTypes("tweet")
                .setQuery(builder)
                .get();

        SearchHits hits = response.getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
    }


    /**
     * bool查询
     */
    @Test
    public void bool() {
        BoolQueryBuilder boolBuilder = new BoolQueryBuilder();
        // AND
        boolBuilder.must(QueryBuilders.termQuery("name", "SIMA"));
        // OR
        boolBuilder.should(QueryBuilders.termQuery("name", "SIMA1"));
        // NOT
        boolBuilder.mustNot(QueryBuilders.termQuery("name", "KILL"));
        // 过滤
        boolBuilder.filter(QueryBuilders.termQuery("price", 2.2D));

        SearchResponse response = client.prepareSearch("twitter")
                .setTypes("tweet")
                .setQuery(boolBuilder)
                .get();

        SearchHits hits = response.getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
    }

    /**
     * script查询
     */
    @Test
    public void script() {
        Script script = new Script("doc['price'].value > 1");
        ScriptQueryBuilder scriptbuilder = new ScriptQueryBuilder(script);

        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.must(scriptbuilder);
        SearchResponse response = client.prepareSearch("twitter")
                .setTypes("tweet")
                .setQuery(boolQueryBuilder)
                .get();

        SearchHits hits = response.getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
            //将获取的值转换成map的形式
            Map<String, Object> map = hit.getSource();
            for (String key : map.keySet()) {
                System.out.println(key + " key对应的值为：" + map.get(key));
            }
        }
    }

}
